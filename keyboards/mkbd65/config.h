// Copyright 2023 XenGi (@XenGi)
// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

// disable debugging
//#define NO_DEBUG
// disable printing/debugging using hid_listen
//#define NO_PRINT
// disable tap dance and other tapping features
//#define NO_ACTION_TAPPING
// disable one-shot modifiers
//#define NO_ACTION_ONESHOT
