// Copyright 2023 XenGi (@XenGi)
// SPDX-License-Identifier: GPL-2.0-or-later

#include QMK_KEYBOARD_H
#include "keymap_german.h"

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
  [0] = LAYOUT( /* Base layer */
    QK_GESC, DE_1,    DE_2,    DE_3,          DE_4,   DE_5,   DE_6,   DE_7,    DE_8,    DE_9,    DE_0,    KC_BSPC, KC_BSPC,
    DE_Q,    DE_W,    DE_E,    DE_R,          DE_T,   DE_Z,   DE_U,   DE_I,    DE_O,    DE_P,    DE_UDIA, DE_PLUS, KC_ENT,
    DE_A,    DE_S,    DE_D,    DE_F,          DE_G,   DE_H,   DE_J,   DE_K,    DE_L,    DE_ODIA, DE_ADIA, DE_HASH, KC_ENT,
    DE_Y,    DE_X,    DE_C,    DE_V,          DE_B,   DE_N,   DE_M,   DE_COMM, DE_DOT,  DE_MINS, KC_UP,   DE_LABK, KC_RSFT,
    KC_LCTL, MO(1),   KC_LSFT, OSM(MOD_RALT), KC_SPC, KC_SPC, KC_SPC, DE_SS,   DE_ACUT, KC_LEFT, KC_DOWN, KC_RGHT, KC_RCTL
  ),

  [1] = LAYOUT( /* Function layer */
    QK_BOOT, KC_F1,   KC_F2,   KC_F3,   KC_F4,   KC_F5,   KC_F6,   KC_F7,   KC_F8,   KC_F9,   KC_F10,  KC_DEL,  KC_DEL,
    _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, KC_F11,  KC_F12,  KC_APP,
    _______, _______, _______, _______, _______, _______, _______, _______, KC_MPLY, KC_MSTP, KC_GRV,  KC_LGUI, KC_LGUI,
    _______, _______, _______, _______, _______, _______, _______, KC_VOLD, KC_VOLU, KC_MUTE, KC_PGUP, _______, _______,
    _______, _______, _______, _______, KC_TAB,  KC_TAB,  KC_TAB,  _______, _______, KC_HOME, KC_PGDN, KC_END,  _______
  )
};
