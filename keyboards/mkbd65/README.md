![mkbd65](https://gitlab.com/mkbd65/qmk-firmware/-/raw/mkbd65/keyboards/mkbd65/mkbd65.png?inline=false)

# mkbd65

A mobile keyboard addon for modern smartphones.

- Keyboard Maintainer: [XenGi](https://xengi.de)
- Hardware Supported: [mkbd65 PCB](https://gitlab.com/mkbd65/pcb)
- Hardware Availability: -

Flashing this keyboard with `en-us-intl` layout (after setting up your build environment):

```
qmk flash -kb mkbd65:rev1 -km en-us-intl
```

Available layouts:

  - en-us-intl (ansi)
  - de (iso)

You can find community provided layouts here: https://gitlab.com/mkbd65/community

See the [build environment setup](https://docs.qmk.fm/#/getting_started_build_tools) and the [make instructions](https://docs.qmk.fm/#/getting_started_make_guide) for more information. Brand new to QMK? Start with our [Complete Newbs Guide](https://docs.qmk.fm/#/newbs).

---

Copyright 2023 XenGi mkbd65@xengi.de @XenGi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
