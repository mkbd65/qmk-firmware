DEFAULT_FOLDER = rev1
# Enables Link Time Optimization (LTO) when compiling the keyboard. This makes the process take longer, but it can
# significantly reduce the compiled size (and since the firmware is small, the added time is not noticeable).
LTO_ENABLE = yes

EXTRAKEY_ENABLE = yes       # Audio control and System control
